``rollingpin``
##############



Summary
=======

``rollingpin`` is a command-line tool that merges two or more
Cookiecutter_ templates into one, avoiding duplication of common content
(because *DRY!*).

One could run ``cookiecutter`` once for each template, using
``--overwrite-if-exists`` and ``--replay`` options, but that's
cumbersome and makes it very difficult to share context data between
runs (unless each required context variable is present in the config of
the first cookiecutter used).  ``rollingpin`` avoids all of this with
one, simple command-line:

.. code:: console

   $ rollingpin -n python common Python manpage dpkg

.. _Cookiecutter: https://github.com/cookiecutter/cookiecutter



Status
======

``rollingpin`` is a work in progress.  It's functional, but rough.
Consider it a (very) Minimum Viable Product.

Immediate future plans:

- (Much) smarter merging algorithms

  - ``cookiecutter.json`` files
  - ``hooks/``
  - template content

- Python packaging
- Debian packaging
- Working Makefile(s)



Copying
=======

Everything here is copyright © 2020-2023 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
