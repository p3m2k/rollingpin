# Top-Level Project Makefile
# (Compatible with GNU make)
#
# Copyright (c) 2017-2022 Paul Mullen (pm@nellump.net)
# SPDX-License-Identifier: MIT


SHELL = /bin/bash
MAKEFLAGS = --warn-undefined-variables

include project.mk
include sequences.mk



# Configuration
#

SRC_DIR = $(CURDIR)/src
META_DIR = $(CURDIR)/meta
ARCHIVE_DIR = $(CURDIR)
DEBIAN_DIR = $(CURDIR)/debian

# For the children!
export CURDIR0 = $(CURDIR)
export SRC_DIR META_DIR

export SUBST_VARS=$(META_DIR)/substvars.sh

# install/uninstall
INSTALL_CFG = $(META_DIR)/install.txt
INSTALL_CFG_FIELDS  =  source dest mode owner group
PREFIX ?= /usr/local
# Set by `debuild`:
DESTDIR ?= $(PREFIX)



# Targets
#

.PHONY: all build test \
		install uninstall \
		release erchive dpkg \
		clean distclean


all: test


build:
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)


test: build
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)


install: test
	$(call echo_heading)
	[[ ! -d $(DESTDIR) ]]  &&  mkdir -p $(DESTDIR)
	$(call export_substvars) ; cat $(INSTALL_CFG) | $(strip_comments) | $(substitute_vars) | \
		$(call install_files,$(SRC_DIR),$(DESTDIR))


uninstall:
	$(call echo_heading)
	$(call export_substvars) ; \
	cat $(INSTALL_CFG) | $(strip_comments) | $(substitute_vars) | while read $(INSTALL_CFG_FIELDS) ; do \
		rm -f $(DESTDIR)/$$dest ; \
	done


release: archive dpkg


archive:
	$(call echo_heading)
# No effect unless on "master" branch.
ifeq ($(GIT_BRANCH),master)
	mkdir -p $(ARCHIVE_DIR)
	git archive v.$(VERSION) --prefix $(NAME)_$(VERSION)/ --output $(ARCHIVE_DIR)/$(NAME)_$(VERSION).tgz
	git archive v.$(VERSION) --prefix $(NAME)_$(VERSION)/ --output $(ARCHIVE_DIR)/$(NAME)_$(VERSION).zip
else
	@echo "Switch to master branch before creating archives!"
endif


# debuild invokes the appropriate target dependencies.
dpkg:
	$(call echo_heading)
	debuild -us -uc -b


clean:
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)
	rm -fr $(BUILD_DIR)


distclean: clean
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)
	git clean -xd --force
