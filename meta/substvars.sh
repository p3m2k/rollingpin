# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="Rollingpin"
SLUG="rollingpin"
DESCRIPTION="A command line tool that merges cookiecutter templates."
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2020-2022"
RELEASE_VERSION="0.1.0a"
RELEASE_DATE="2023-02-10"
