# Project-Specific Configuration
#

# Meta
export NAME = $(shell $(call get_subst_var,SLUG))
export VERSION = $(shell $(call get_subst_var,RELEASE_VERSION))
export GIT_BRANCH = $(shell git branch --show-current)
export GIT_HEAD=$(shell git log -1 --pretty=format:%h)

# Make Targets
# (If it's not listed here, it's invisible to the Makefile's "build" and
# "clean" targets.)
export PYTHON   = rollingpin
export MANPAGES = rollingpin.1.gz
