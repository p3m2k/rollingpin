from setuptools import setup


setup(
    name = 'rollingpin',
    version = '0.1.0',
    packages = ['rollingpin'],
    entry_points = {
        'console_scripts': [
            'rollingpin = rollingpin.__main__:main'
        ]
    })
