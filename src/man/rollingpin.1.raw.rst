#######
${SLUG}
#######

${DESCRIPTION}
==============

:author: ${AUTHOR_NAME}
:contact: ${AUTHOR_CONTACT}
:copyright: © ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} <${COPYRIGHT_CONTACT}>
:manual_section: FIXME
:version: ${RELEASE_VERSION}
:date: ${RELEASE_DATE}

.. From `man man`:

.. man page sections:
..   1   Executable programs or shell commands
..   2   System calls (functions provided by the kernel)
..   3   Library calls (functions within program libraries)
..   4   Special files (usually found in /dev)
..   5   File formats and conventions, e.g. /etc/passwd
..   6   Games
..   7   Miscellaneous  (including  macro packages and conventions), e.g. man(7), groff(7),
..       man-pages(7)
..   8   System administration commands (usually only for root)
..   9   Kernel routines [Non standard]

.. Conventional section names include NAME,  SYNOPSIS,  CONFIGURATION,
.. DESCRIPTION,  OPTIONS, EXIT STATUS, RETURN VALUE, ERRORS, ENVIRONMENT,
.. FILES, VERSIONS, CONFORMING TO, NOTES, BUGS, EXAMPLE, AUTHORS, and SEE
.. ALSO.



Synopsis
--------

**TODO**

${SLUG} [-h] [-v] [-V] [-n]



Description
-----------

**TODO**

${DESCRIPTION}



Options
-------

**TODO**

-h
    Print usage.
-v
    Print version.
-V
    Verbose mode
-n
    Dry run mode



Files
-----

**TODO**



Environment
-----------

**TODO**



Exit Status
-----------

**TODO**

0
    Success
1
    Failure
2
    File I/O errors
3
    Usage errors



Notes
-----

**TODO**



Examples
--------

**TODO**



Bugs & Known Limitations
------------------------

**TODO**



See Also
--------

**TODO**
