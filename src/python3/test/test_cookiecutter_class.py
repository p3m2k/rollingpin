import pytest

from pathlib import Path
from shutil import rmtree

from rollingpin.cookiecutter import Cookiecutter


# FIXME: Delete this!
#
# def test_zero_division():
#     with pytest.raises(ZeroDivisionError):
#         1 / 0
#
#
# def test_recursion_depth():
#     with pytest.raises(RuntimeError) as excinfo:
#
#         def f():
#             f()
#
#         f()
#     assert "maximum recursion" in str(excinfo.value)


class TestCookiecutterClass:

    def test_instantiate_with_defaults(self, tmp_path, get_fake_cookiecutter):
        fake_cc_path = get_fake_cookiecutter
        cookiecutter_name = 'fake_cookiecutter'
        cc = Cookiecutter(tmp_path, cookiecutter_name)

        assert isinstance(cc, Cookiecutter)
        assert fake_cc_path.exists()


    def test_instantiate_new(self, tmp_path):
        cookiecutter_name = 'fake_cookiecutter'
        cookiecutter_dir_path = tmp_path/cookiecutter_name

        cc = Cookiecutter(tmp_path, cookiecutter_name, is_new=True)

        assert isinstance(cc, Cookiecutter)
        assert cookiecutter_dir_path.exists()




# __init__()
# - test override of template_dir_name
# - is_new=True and self.path.exists() == True and clobber=False --> exception raised
# - is_new=True and self.path.exists() == True and clobber=True --> # rmtree() called
# - is_new=True and self.path.exists() == False --> _initialize() called
# - is_new=False and self.path.exists() == False --> exception raised
# - is_new=False and self.path.is_dir() == False --> exception raised
# - is_new=False --> _validate(), _load_config(), _load_hooks() called

# _initialize()
# - _write_config() called
# - verify on-disk file system structure
#   - self.path/config_file_name (empty)
#   - self.path/hooks_dir_name
#   - self.path/template_dir_name

# _validate()
# - self.config_path.exists == False
# - self.config_path.is_file == False
# - self.template_dir_path.exists == False
# - self.template_dir_path.is_dir() == False

# _load_config()
# - load test YAML (unnecessary?)

# _write_config()
# - write test YAML (unnecessary?)

# _merge_config()
# - test YAMLs w/ 1+ common entries (other over-writes self)
# - test YAMLs w/ no common entries

# _load_hooks()
# - create fake cookiecutter w/ assortment of hook scripts
#   - *.py, *.sh, *.txt
#   - confirm only *.py and *.sh loaded

# _write_hooks()
# - create fake cookiecutter w/ hook scripts
#   - confirm all self.hooks entries written to file

# _merge_hooks()
# - create fake cookiecutters w/ hook scripts
#   - like-named self.hooks entries are concatenated (self+other)
#   - hooks only in other appear in self, in full
#   - empty hooks in other are ignored
#   - _write_hooks() is called

# _merge_template()

# __del__()
# - create cookiecutter instance w/ self.turds pointing at real files/dirs
#   - verify __del__() removes file sys objects pointed to by self.turds #
#     entries

# __repr()__
# - create fake cookiecutter instance
#   - confirm print() outputs correct string

# __add()__

# __iadd()__
