from pytest import fixture

from pathlib import Path
from shutil import rmtree



@fixture
def get_fake_cookiecutter(tmp_path):
    fake_cc_path = tmp_path/'fake_cookiecutter'
    fake_cc_path.mkdir()

    with open(fake_cc_path/'cookiecutter.json', 'w') as fh:
        fh.write('{\n    "cc_name": "%s"\n}')

    template_dir_path = fake_cc_path/'{{ cookiecutter.project_slug }}'
    template_dir_path.mkdir()

    hooks_dir_path = fake_cc_path/'hooks'
    hooks_dir_path.mkdir()

    return fake_cc_path



@fixture
def get_expected_usage_short():
        return b"""Usage:
    rollingpin [-v] [-C] [-i PATH] [-o PATH] [-n NAME] [-t NAME] CC_NAME...
    rollingpin --help
    rollingpin --version
"""


@fixture
def get_expected_usage_long():
        return b"""rollingpin: combine multiple cookiecutters into one

Usage:
    rollingpin [-v] [-C] [-i PATH] [-o PATH] [-n NAME] [-t NAME] CC_NAME...
    rollingpin --help
    rollingpin --version

Arguments:
    CC_NAME                            List of names of source cookiecutters

Options:
    -h --help                          Display program usage and exit
    -v --verbose                       Enable verbose output
    -V --version                       Display program version and exit
    -C --clobber                       Overwrite existing cookiecutter
    -i PATH --input-dir=PATH           Path to input directory
    -o PATH --output-dir=PATH          Path to output directory
    -n NAME --merged-name=NAME         Name of merged cookiecutter
    -t NAME --template-dirs-name=NAME  Name of template directories
                                       [default: {{ cookiecutter.project_slug }}]

By default, input will be read from, and written to, your cookiecutter
directory (~/.cookiecutters/ or whatever is set in your cookiecutterrc file).

By default, the new cookiecutter's name will be a concatenation of the source
cookiecutters' names, separated by a plus sign ("+").
"""
