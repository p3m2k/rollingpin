import pytest

import sys
from subprocess import run


@pytest.mark.skip
class TestRollingpinCommand:

    def test_opts_none(self, get_expected_usage_short):
        expected_stderr = get_expected_usage_short
        r = run(['python3', 'bin/rollingpin'], capture_output=True)
        assert r.stderr == expected_stderr


    def test_opts_help(self, get_expected_usage_long):
        expected_stdout = get_expected_usage_long
        r = run(['python3', 'bin/rollingpin', '--help'], capture_output=True)
        assert r.stdout == expected_stdout
