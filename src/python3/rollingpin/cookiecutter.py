from pathlib import Path
from shutil import copy2, rmtree
from tempfile import mkdtemp
import json



class Cookiecutter:

    """Represents a cookiecutter (JSON config file, hooks directory, and
    template directory)."""

    config_file_name = 'cookiecutter.json'
    hooks_dir_name = 'hooks'
    template_dir_name = '{{ cookiecutter.project_slug }}'


    def __init__(self, parent_dir_path, name,
                 template_dir_name=None, is_new=False, clobber=False):

        self.parent_dir_path = Path(parent_dir_path).expanduser()
        self.name = name

        self.path = self.parent_dir_path/self.name
        self.config_path = self.path/self.config_file_name
        self.hooks_dir_path = self.path/self.hooks_dir_name

        if template_dir_name:
            self.template_dir_path = self.path/template_dir_name
        else:
            self.template_dir_path = self.path/self.template_dir_name

        self.config = {}
        self.hooks = {}
        self.turds = []

        if is_new:
            if self.path.exists():
                if not clobber:
                    raise ValueError("Directory or file already exists: %s" % self.path)
                else:
                    rmtree(self.path)
            else:
                self._initialize()
        else:
            if not self.path.exists():
                raise ValueError("Directory does not exist: %s" % self.path)
            elif not self.path.is_dir():
                raise ValueError("Not a directory: %s" % self.path)
            else:
                self._validate()
                self._load_config()
                self._load_hooks()


    def _initialize(self):

        """Create file system structure for new cookiecutter."""

        self.path.mkdir(parents=True)
        self.template_dir_path.mkdir()
        self.hooks_dir_path.mkdir()
        self._write_config()


    def _validate(self):

        """Ensure that object represents a real cookiecutter template."""

        if not self.config_path.exists() or not self.config_path.is_file():
            raise ValueError("cookicutter.json not found in %s." % self.path)

        if not self.template_dir_path.exists() or not self.template_dir_path.is_dir():
            raise ValueError("Template dir not found: %s" % self.template_dir_path)


    def _load_config(self):

        """Load JSON contents of ``self.config_path`` into ``self.config``."""

        with self.config_path.open() as f:
            self.config = json.load(f)


    def _write_config(self):

        """Convert ``self.config`` to JSON and write to self.config_path``."""

        with self.config_path.open(mode='w') as f:
            #json.dump(self.config, f, sort_keys=True, indent=2)
            json.dump(self.config, f, indent=2)


    def _merge_config(self, other):

        """Merge contents of the ``other`` cookiecutter config with ours."""

        # NB: other.config clobbers same-named keys in self.config!
        # TODO Smarter merge algorithm!
        #  - Option to specify precedence?
        self.config.update(other.config)
        self._write_config()


    def _load_hooks(self):

        """Load contents of all files found in ``self.hooks_path`` into
        ``self.hooks`` dict."""

        hook_files_paths = [x for x in self.hooks_dir_path.glob('*.py')]
        hook_files_paths.extend([x for x in self.hooks_dir_path.glob('*.sh')])

        for f in hook_files_paths:
            with f.open() as fh:
                self.hooks[f.name] = fh.read()


    def _write_hooks(self):

        """Write value of all entries in ``self.hooks`` dict to files in
        ``self.hooks_path``."""

        for k, v in self.hooks.items():
            with open(self.hooks_dir_path/k, 'w') as fh:
                fh.write(v)


    def _merge_hooks(self, other):

        """Merge all hook files in ``self`` with those of ``other``."""

        for filename, contents in other.hooks.items():
            if len(contents) > 0:
                if filename not in self.hooks:
                    # Create empty entry to append to:
                    self.hooks[filename] = ''
                else:
                    # Insert a few newlines before we append:
                    self.hooks[filename] += "\n\n\n"
                # Would be nice to insert an attribution comment here, but if
                # only ``other`` includes the hook, the comment will appear
                # before the shebang, breaking the script.
                self.hooks[filename] += other.hooks[filename]

        self._write_hooks()


    def _merge_template(self, other):

        """Merge contents of other cookiecutter's template dir."""

        for x in other.template_dir_path.glob('**/*'):
            xrel = x.relative_to(other.template_dir_path)
            if x.is_dir():
                (self.template_dir_path/xrel).mkdir(parents=True, exist_ok=True)
            elif x.is_file():
                if not (self.template_dir_path/xrel).exists():
                    copy2(x, self.template_dir_path/xrel, follow_symlinks=False)
                else:
                    # TODO: Rename self copy of file w/ self.name appended to file name?
                    copy2(x, (self.template_dir_path/("%s.%s" % (str(xrel), other.name))))
            else:
                # Well, how did I get here?
                raise ValueError("What do I do with this? : %s" % x)


    def __del__(self):

        """Delete cookiecutter template's file system structure."""

        # NB: Be careful!  Only delete temporary files created by __add__().
        for x in self.turds:
            shutil.rmtree(x)


    def __repr__(self):

        """Return string representation of self."""

        return "<cookiecutter '%s' at %s>" % (self.name, self.path)


    def __add__(self, other):

        """Merge ``self`` with ``other`` object and return as new Cookiecutter.
        (Non-destructive merge.)"""

        if not isinstance(other, Cookiecutter):
            raise TypeError("Not a Cookiecutter: %s" % other)

        tmp_dir_path = mkdtemp()
        self.turds.extend(tmp_dir_path)

        new_cc_name = self.path.name + '_' + other.path.name
        new_cc = Cookiecutter(self.temp_dir_path, new_cc_name, is_new=True)

        new_cc._merge_template(self)
        new_cc._merge_template(other)

        new_cc._merge_config(self)
        new_cc._merge_config(other)

        new_cc._merge_hooks(self)
        new_cc._merge_hooks(other)

        return new_cc


    def __iadd__(self, other):

        """Merge ``self`` with ``other`` object and return self. (Destructive
        merge.)"""

        #  Make changes to, and return, self.

        if not isinstance(other, Cookiecutter):
            raise TypeError("Not a Cookiecutter: %s" % other)

        self._merge_template(other)
        self._merge_config(other)
        self._merge_hooks(other)

        return self
