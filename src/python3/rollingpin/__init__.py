# -*- coding: UTF-8 -*-


# Copyright © 2020-2022 Paul Mullen <pm@nellump.net>
# SPDX-License-Identifier: MIT



import sys, os


__NAME__      = os.path.basename(sys.argv[0])
__version__   = "0.1.0"
__author__    = "Paul Mullen"
__email__     = "pm@nellump.net"
__copyright__ = "2020-2022"
__license__   = "MIT"
__all__ = []
