"""
rollingpin: combine multiple cookiecutters into one

Usage:
    rollingpin [-v] [-C] [-i PATH] [-o PATH] [-n NAME] [-t NAME] CC_NAME...
    rollingpin --help
    rollingpin --version

Arguments:
    CC_NAME                            List of names of source cookiecutters

Options:
    -h --help                          Display program usage and exit
    -v --verbose                       Enable verbose output
    -V --version                       Display program version and exit
    -C --clobber                       Overwrite existing cookiecutter
    -i PATH --input-dir=PATH           Path to input directory
    -o PATH --output-dir=PATH          Path to output directory
    -n NAME --merged-name=NAME         Name of merged cookiecutter
    -t NAME --template-dirs-name=NAME  Name of template directories
                                       [default: {{ cookiecutter.project_slug }}]

By default, input will be read from, and written to, your cookiecutter
directory (~/.cookiecutters/ or whatever is set in your cookiecutterrc file).

By default, the new cookiecutter's name will be a concatenation of the source
cookiecutters' names, separated by a plus sign ("+").
"""


# Copyright (c) 2020-2022 Paul Mullen (pm at nellump dot net).
# SPDX-License-Identifier: MIT



import sys

from docopt import docopt
from cookiecutter.config import get_user_config
from pathlib import Path

from . import __NAME__, __version__
from .cookiecutter import Cookiecutter



def main(argv):

    # Parse command line.
    #

    opts = docopt(__doc__, argv=argv, version=__version__)
    # FIXME: Delete these:
    # print("%s %s" % (__NAME__, __version__))
    # print(opts, file=sys.stderr)


    # Set input and output directory paths.
    #

    # Get cookiecutters directory from user config.
    cookiecutter_config = get_user_config(config_file=None, default_config=False)
    cookiecutters_dir = Path(cookiecutter_config['cookiecutters_dir']).expanduser()

    if opts['--input-dir'] is None:
        input_dir = cookiecutters_dir
    else:
        input_dir = Path(opts['--input-dir']).expanduser()

    if opts['--output-dir'] is None:
        output_dir = cookiecutters_dir
    else:
        output_dir = Path(opts['--output-dir']).expanduser()


    # Merge cookiecutters.
    #

    if opts['--merged-name'] is None:
        new_cookiecutter_name = '+'.join(opts['CC_NAME'])
    else:
        new_cookiecutter_name = opts['--merged-name']

    new_cookiecutter = Cookiecutter(output_dir, new_cookiecutter_name,
                                    opts['--template-dirs-name'], is_new=True,
                                    clobber=opts['--clobber'])
    for src_cookiecutter_name in opts['CC_NAME']:
        new_cookiecutter += Cookiecutter(input_dir, src_cookiecutter_name,
                                         opts['--template-dirs-name'],
                                         clobber=opts['--clobber'])





if __name__ == "__main__":

    sys.exit(main(sys.argv[1:]))
